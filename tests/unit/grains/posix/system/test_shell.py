import os
from unittest import mock

import pytest


@pytest.mark.asyncio
async def test_load_shell(mock_hub, hub):
    ret = r"/bin/bash"
    with mock.patch.dict(os.environ, {"SHELL": ret}):
        mock_hub.grains.posix.system.shell.load_shell = (
            hub.grains.posix.system.shell.load_shell
        )
        await mock_hub.grains.posix.system.shell.load_shell()
    assert mock_hub.grains.GRAINS.shell == ret
